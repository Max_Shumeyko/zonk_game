#ifndef __TOOLS_
#define __TOOLS_

/*
    Get host ip address as char array (memory should be freed)
*/
char*
get_my_ip(void);

/*
    Bind handler to signal sig.
*/
int
catch_signal(int sig, void (*handler)(int));

/*
    Read message safely from given socket. (using loop)
*/
int
read_in(int socket, char* buf, int len);

/*
    Show error message and exit.
*/
void
error(char* msg);

/*
    Open reusable INET6 stream socket. Set to nonblocking for async server.
*/
int
open_listener_socket(void);

void
bind_to_port(int socket, int port);

/*
    Send message to socket.
*/
int
say(int socket, char* s);

#endif
