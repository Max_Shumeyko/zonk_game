#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/socket.h>
#include <sys/poll.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "crypto_tools.h"
#include "tools.h"
#include "game.h"


#define RCV_SIZE 80
#define FM_SIZE 4095

int 
cmp_players_sock(const void* player1, const void* player2)
{
    player_t pl1 = *(player_t* ) player1;
    player_t pl2 = *(player_t* ) player2;
    if (pl1.sock_fd == pl2.sock_fd) return 0;
    return pl1.sock_fd < pl2.sock_fd ? 1 : -1;
}


int
sort_players(player_t* players, int size)
{
    qsort(players, size, sizeof(player_t), cmp_players_sock);

    for (int i = 0; i < size; i++)
        if (players[i].sock_fd == 0)
            return i;

    return 0;
}


int
collect_connected_players(
    struct pollfd* fds,
    int listener_sd, 
    player_t* players, int p_num)
{
    // waiting for connection after host sends tokens to players (somehow)    
    memset(fds, 0, sizeof(struct pollfd) * 40);

    int nfds = 1; // number of tracking events
    int i;
    fds[0].fd = listener_sd;
    fds[0].events = POLLIN;
    
    char rcv_buf[RCV_SIZE];
    char token_buf[20];
    char name_buf[40];
    int timeout = 120 * 1000; // 120 seconds for connection
    printf("Waiting %d for connection ...\n", timeout / 1000);

    int connected = 0;
    while (true) // run server to collect clients list
    {
        if (connected == p_num)
        {
            printf("All players connected!\n");
            break;
        }

        int rc = poll(fds, nfds, timeout);
        if (rc < 0)
        {
            perror("poll");
            continue;
        }
        else if (rc == 0)
        {
            perror("Poll timeout");
            break;
        }

        if (fds[0].revents == POLLIN)
        {
            while (true)
            {
                int new_sd = accept(listener_sd, NULL, NULL);
                if (new_sd < 0) 
                    break;

                say(new_sd, "Hello!\n");
                read_in(new_sd, rcv_buf, RCV_SIZE);
                sscanf(rcv_buf, "%[^:]: %[^\n]", &token_buf, &name_buf);
                
                i = check_token(token_buf, players, p_num); 
                if (i == -1)
                    continue;

                // fill players data
                int len = strlen(name_buf);
                players[i].name = (char* ) malloc(len + 1);
                strncpy(players[i].name, name_buf, len - 1);
                players[i].name[len - 1] = '\0';
                players[i].sock_fd = new_sd;

                printf("Player connected: %s\n", players[i].name);

                connected += 1;

            } // while (true) accept

            timeout = 90 * 1000; // 90 sec to wait
        } // if POLLIN

    } // while (true) poll

    return connected;
}


int
send_to_game_app(
    int to_game, int from_game, char* msg, int size,
    char* msg_out, int size_out
)
{
    char yes[] = "y\n";
    char no[] = "n\n";
    if (strstr(msg, "y"))
        write(to_game, yes, strlen(yes));
    else if (strstr(msg, "n"))
        write(to_game, no, strlen(no));
    else 
        return - 1;

    usleep(1000);
    int res = read_gm(from_game, msg_out, size_out);
    return res;
}

int
chat_and_play_game(
    struct pollfd* fds,
    int to_game, int from_game,
    int* cpl,
    player_t* players, int p_num,
    int max_score
)
{
    // waiting for connection after host sends tokens to players (somehow)    
    memset(fds, 0, sizeof(struct pollfd) * 40);

    // listen to connected players
    int i;
    for (i = 0; i < p_num; i++)
    {   
        fds[i].fd = players[i].sock_fd;
        fds[i].events = POLLIN;
    }
    
    char sock_msg[RCV_SIZE];
//    char full_msg[RCV_SIZE + 40 + 1];
    char full_msg[FM_SIZE];

    // start game
    int current_player = 0;

    usleep(1000);
    read_gm(from_game, full_msg, sizeof(full_msg));
    read_gm(from_game, full_msg, sizeof(full_msg));

    memset(full_msg, 0, FM_SIZE);
    snprintf(full_msg, FM_SIZE, 
        "\nGAME CONDITION: %d PLAYERS, MAX SCORE: %d"
        "\nTURN: %s"
        "\nDo you want to throw cubes? [y/n]\n",
        p_num, max_score, players[current_player].name
    );
    echo_to_all(full_msg, players, p_num);

    // poll circle
    int timeout = -1; // poll waits for endless 
    while (true) // run server to collect clients list
    {
        if (current_player == p_num)
            current_player = 0;
//        printf("PLAYER'S %s TURN\n", players[*current_player].name);

        int rc = poll(fds, p_num, timeout);
        if (rc < 0)
        {
            perror("poll");
            break;
        }
        else if (rc == 0)
        {
            perror("Poll timeout");
            break;
        }

        memset(sock_msg, 0, RCV_SIZE);
        memset(full_msg, 0, FM_SIZE);
        for (i = 0; i < p_num; i++)
            if (fds[i].revents == POLLIN)
            {
                
                read_in(fds[i].fd, sock_msg, RCV_SIZE);
                if (i != current_player) // chat
                {
                    snprintf(
                        full_msg, sizeof(full_msg), 
                        "%s :: %s\n", players[i].name, sock_msg);
                    echo_to_all(full_msg, players, p_num);

                    memset(sock_msg, 0, RCV_SIZE);
                    memset(full_msg, 0, FM_SIZE);
                } // chat
                else // current player's turn
                {
                    // send message and receive a response
                    printf("PLAYER %s: \n%s\n", players[i].name, sock_msg);
                    send_to_game_app(
                        to_game, from_game, 
                        sock_msg, RCV_SIZE,
                        full_msg, FM_SIZE
                    );

                    printf("\nGAME:\n%s\n", full_msg);

                    echo_to_all(full_msg, players, p_num);
                    if (strstr(full_msg, "NEXT"))
                        current_player++;

                    if (strstr(full_msg, "WINS"))
                        return 0;

                    memset(sock_msg, 0, RCV_SIZE);
                    memset(full_msg, 0, FM_SIZE);
                } // player's turn

            } // if POLLIN

    } // while (true) poll

    return 0;
}


void
echo_to_all(char* msg, player_t* players, int p_num)
{
    for (int i = 0; i < p_num; i++)
    {
        int res = say(players[i].sock_fd, msg);
        if (res == -1)
            printf("Player %s can't receive the message!\n",
                players[i].name);
    }
}


void 
free_tokens(char** tokens, int p_num)
{
    if (tokens == NULL)
        return;

    for (int i = 0; i < p_num; i++)
        if (tokens[i] != NULL)
        {   
            free(tokens[i]);
            tokens[i] = NULL;
        }
    free(tokens);
    tokens = NULL;
}
    

void
free_players(player_t* players, int p_num)
{
    for (int i = 0; i < p_num; i++)
        if (players[i].name != NULL) {
            free(players[i].name);
            players[i].name = NULL;
        }
}


int
print_from_fd(int fd)
{
    char buf[4096];

    while (true)
    {
        ssize_t count = read(fd, buf, sizeof(buf));
        if (count == -1) 
        {
            if (errno == EINTR)
                return -1;
            else 
                error("read");
        }
        else if (count == 0) 
            return 0;
        else 
        {
            buf[count] = '\0';
            printf("%s\n", buf);
            return count;
        }
    }
}

int
read_gm(int fd, char* buf, int size)
{
    fsync(fd);
    memset(buf, 0, size);

    int res;
    int total = 0;

    while (size != 0 && (res = read(fd, buf, size)) != 0 )
    {
        if (res == -1) {
            if (errno == EINTR)
                continue;

            if (errno == EAGAIN)
                break;

            perror("read");
            break;
        }

        if (res == 0)
            break;
        
        size -= res;
        buf += res;
        total += res;
    }
    
    buf[total] = '\n';
    return total;
}

int
write_gm(int fd, char* msg, int size)
{
    char buf[FM_SIZE];
    memset(buf, 0, size);
    strncpy(buf, msg, size);
    return write(fd, buf, size);
}


void
init_game(
    int to_game, int from_game, 
    int max_score, 
    player_t* players, int p_num
)
{
    char msg[FM_SIZE];
    // init game conditions
    write(to_game, "y\n", strlen("y\n"));

    read_gm(from_game, msg, sizeof(msg));
    printf(">> %s\n", msg);

    memset(msg, 0, sizeof(msg));
    snprintf(msg, sizeof(msg), "%d\n", p_num);
    write(to_game, msg, strlen(msg));

    read_gm(from_game, msg, sizeof(msg));
    printf(">> %s\n", msg);

    for (int i = 0; i < p_num; i++)
    {
        memset(msg, 0, sizeof(msg));
        snprintf(msg, strlen(players[i].name) + 2, "%s\n", players[i].name);
        write(to_game, msg, strlen(msg));
    }

    memset(msg, 0, sizeof(msg));
    snprintf(msg, sizeof(msg), "%d\n", max_score);
    write(to_game, msg, strlen(msg));

}
