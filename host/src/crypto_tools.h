#ifndef __CRYPTO__
#define __CRYPTO__

#include <stdbool.h>

#include "game.h"

typedef struct node_t {
    char* key;
    char* iv;
} node_t;


void
init_node_t(node_t* node);

void 
free_node_t(node_t* node);

unsigned char*
gen_key(unsigned int len);

char* 
create_token(int num, char* ip, int port);

int
check_token(char* msg, player_t* players, int size);

void 
handleErrors(void);                                                          

int 
encrypt(unsigned char *plaintext, int plaintext_len,
        unsigned char *key, unsigned char *iv, unsigned char *ciphertext);                                                                                                             

int 
decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,  
        unsigned char *iv, unsigned char *plaintext);


#endif 
