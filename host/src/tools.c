#include <stdio.h>                                                               
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include <sys/socket.h>
#include <sys/poll.h>
#include <sys/time.h>
#include <sys/ioctl.h>

#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "tools.h"


#define SERVER_PORT 30000


char*
get_my_ip(void)
{
    char hostbuf[256];
    char* IPaddr;
    struct hostent *host_entry;

    if (gethostname(hostbuf, sizeof(hostbuf)) == -1)
        error("gethostname");

    host_entry = gethostbyname(hostbuf);
    if (host_entry == NULL)
        error("gethostbyname");

    IPaddr = inet_ntoa(
        *((struct in_addr* ) host_entry->h_addr_list[0])
    );

    return IPaddr;
}


int
catch_signal(int sig, void (*handler)(int))
{
    struct sigaction action;
    action.sa_handler = handler;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    return sigaction(sig, &action, NULL);
}


int
read_in(int socket, char* buf, int len)
{
    char* s = buf;
    int slen = len;
    int c = recv(socket, s, slen, 0);
    while ((c > 0) && (s[c - 1] != '\n'))
    {
        s += c;
        slen -= c;
        c = recv(socket, s, slen, 0);
    }

    if (c < 0)
        return c;
    else if (c == 0)
        buf[0] = '\0';
    else
        s[c - 1] = '\0';
    
    return len - slen;
}


void
error(char* msg)
{
    fprintf(stderr, "%s: %s\n", msg, strerror(errno));
    raise(SIGINT);
    exit(1);
}


int
open_listener_socket(void)
{
    int sd = socket(AF_INET6, SOCK_STREAM, 0);
    if (sd == -1)
        error("open_listener_socket: socket");

    // allow socket to be reuseble
    int on = 1;
    int i = setsockopt(
        sd, 
        SOL_SOCKET, SO_REUSEADDR,
        (char*) &on, sizeof(on)
    );
    if (i < 0)
        error("open_listener_socket: setsockopt");

    // set socket to be nonblocking
    i = ioctl(sd, FIONBIO, (char*) &on);
    if (i < 0)
        error("open_listener_socket: ioctl");

    return sd;
}  


void
bind_to_port(int socket, int port)
{
    struct sockaddr_in6 addr;
    addr.sin6_family = AF_INET6;
    addr.sin6_port = (in_port_t)htons(port);
    memcpy(&addr.sin6_addr, &in6addr_any, sizeof(in6addr_any));

    int c = bind(socket, (struct sockaddr*) &addr, sizeof(addr));
    if (c == -1)
        error("bind");
}


int
say(int socket, char* s)
{
    int result = send(socket, s, strlen(s), 0);
    if (result == -1)
    {
        printf("ERROR OCCURED!\n");
        fprintf(stderr, "%s: %s\n", "send", strerror(errno));
    }
    return result;
}


