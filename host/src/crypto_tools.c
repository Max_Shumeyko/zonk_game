#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>

#include "crypto_tools.h"
#include "tools.h"
#include "game.h"

unsigned char*
gen_key(unsigned int len)
{
    uint32_t seed = 0;
    FILE *devrnd = fopen("/dev/random", "r");
    fread(&seed, 4, 1, devrnd);
    fclose(devrnd);
    srand(seed);

    unsigned char* key = malloc(len + 1);
    for (int i = 0; i < len; i++)
        key[i] = rand() % 10 + '0'; 
    key[len] = '\0';

    return key;
}


char*
create_token(int num, char* ip, int port)
{
    char* key = gen_key(16);
    
    int size = strlen(key) + strlen(ip) + 5 + 7; // 5 - port, 7 - symbols # $
    char* token = (char*)malloc(size);
    
    // player's number # token key # ip address # port
    snprintf(token, size, "%d#%s#%s#%d$", num, key, ip, port); 

    free(key);

    return token;
}


int
check_token(char* msg, player_t* players, int size)
{
    for (int i = 0; i < size; i++)
        if (strstr(players[i].key, msg) != NULL)
            return i;

    return -1;
} 

    
void
init_node_t(node_t* node)
{
    node->key = gen_key(32);
    node->iv = gen_key(16);
}
    
void
free_node_t(node_t* node)
{
    free(node->key);
    free(node->iv);
}

    
void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}

int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
            unsigned char *iv, unsigned char *ciphertext)
{
    EVP_CIPHER_CTX *ctx;

    int len;

    int ciphertext_len;

    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new()))
        handleErrors();

    /*
     * Initialise the encryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits
     */
    if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
        handleErrors();

    /*
     * Provide the message to be encrypted, and obtain the encrypted output.
     * EVP_EncryptUpdate can be called multiple times if necessary
     */
    if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
        handleErrors();
    ciphertext_len = len;

    /*
     * Finalise the encryption. Further ciphertext bytes may be written at
     * this stage.
     */
    if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
        handleErrors();
    ciphertext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return ciphertext_len;
}


int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
            unsigned char *iv, unsigned char *plaintext)
{
    EVP_CIPHER_CTX *ctx;

    int len;

    int plaintext_len;

    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new()))
        handleErrors();

    /*
     * Initialise the decryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits
     */
    if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
        handleErrors();

    /*
     * Provide the message to be decrypted, and obtain the plaintext output.
     * EVP_DecryptUpdate can be called multiple times if necessary.
     */
    if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
        handleErrors();
    plaintext_len = len;

    /*
     * Finalise the decryption. Further plaintext bytes may be written at
     * this stage.
     */
    if(1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len))
        handleErrors();
    plaintext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return plaintext_len;
}
