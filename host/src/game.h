#ifndef __GAME_T__
#define __GAME_T__

typedef struct {
    int id;
    char* key;
    char* name;
    int sock_fd;
} player_t;

int 
cmp_players_sock(const void* player1, const void* player2);

int
sort_players(player_t* players, int size);

int
collect_connected_players(
    struct pollfd* fds,
    int listener_sd, 
    player_t* players, int p_num
);

int
chat_and_play_game(
    struct pollfd* fds,
    int to_game, int from_game,
    int* current_player,
    player_t* players, int p_num,
    int max_score
);

void
echo_to_all(char* msg, player_t* players, int p_num);

void 
free_tokens(char** tokens, int p_num);

void 
free_players(player_t* players, int player_num);

int
print_from_fd(int fd);

int
read_gm(int fd, char* buf, int size);

int
write_gm(int fd, char* buf, int size);

int
start_game(char* app_addr, int listener_sd, int* from_game, int* to_game);

void
init_game(
    int to_game, int from_game, 
    int max_score, 
    player_t* players, int p_num
);

int
send_to_game_app(
    int to_game, int from_game, char* msg, int size,
    char* msg_out, int size_out
);

#endif
