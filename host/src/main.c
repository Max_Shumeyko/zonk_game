#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>

#include <sys/socket.h>
#include <sys/poll.h>

#include "crypto_tools.h"
#include "tools.h"
#include "game.h"


#define PORT 30000
#define PLAYERS_NUM 10
#define LINE_SIZE 80

int listener_sd;
int current_player = 0;


void
handle_shutdown(int signal)
{
    if (listener_sd)
        close(listener_sd);

    perror(" >> Interrupted >> Socket closed ");
    exit(0);
}


void
handle_alarm(int signal)
{
    current_player++;
}


int
main(void)
{
    // handle CTRL+C
    if (catch_signal(SIGINT, handle_shutdown) == -1)
    {
        perror("catch_signal SIGINT");
        exit(1);
    }

    // handle SIGALRM
    if (catch_signal(SIGALRM, handle_alarm) == -1)
    {
        perror("catch_signal SIGALRM");
        exit(1);
    }

    // init online players's number
    int p_num;
    printf("Input the number of players [2-10]: ");
    if (scanf("%d", &p_num) == 0)
        error("scanf");
    
    if (p_num < 2)
        p_num = 2;
    else if (p_num > 10)
        p_num = 10;


    // init max score
    int max_score;
    printf("Input the score limit [1000 - 30000]: ");
    if (scanf("%d", &max_score) == 0)
        error("scanf");

    if (max_score < 1000)
        max_score = 1000;

    if (max_score > 30000)
        max_score = 30000;
        
    // create p_num tokens
    int i;
    char** tokens = (char**) malloc(p_num * sizeof(char*));

    char ip_addr[80]; // host ip
    strncpy(ip_addr, get_my_ip(), 80); //sizeof(get_my_ip()) );
    
    for (i = 0; i < p_num; i++) {
        tokens[i] = create_token(i, ip_addr, PORT);
        printf("token %d: %s\n", i, tokens[i]);
    }

    // init players array
    player_t players[PLAYERS_NUM];
    memset(players, 0, sizeof(player_t) * 10);

    for (i = 0; i < p_num; i++)
    {
        players[i].id = i;
        players[i].key = tokens[i];
    }

    // run host server
    listener_sd = open_listener_socket();
    bind_to_port(listener_sd, PORT);
    if (listen(listener_sd, p_num) == -1)
        error("listen");

    struct pollfd fds[40];
    int connected = collect_connected_players(fds, listener_sd, players, p_num);
    if (connected < 2)
        error("Not enough players to play game!");

    printf("CONNECTED: %d of %d total\n", connected, p_num);

    if (connected < p_num)
    {   
        p_num = sort_players(players, p_num);
    }
    // show connected
    printf("Players have connected: \n");
    for (i = 0; i < p_num; i++)
        printf("%s %d\n", players[i].name, players[i].sock_fd);

    // start game in isolated process
    pid_t pid = 0;
    int inpipefd[2];
    int outpipefd[2];
    char buf[256];

    if (pipe(inpipefd) == -1)
        error("pipe in");
    if (pipe(outpipefd) == -1)
        error("pipe out");
    int to_game = outpipefd[1];
    int from_game = inpipefd[0];

    if (fcntl(from_game, F_SETFL, O_NONBLOCK) == -1)
        error("fcntl");

    pid = fork();
    if (pid == -1)
        error("fork");

    if (!pid)
    {
        dup2(outpipefd[0], STDIN_FILENO);
        dup2(inpipefd[1], STDOUT_FILENO);
        dup2(inpipefd[1], STDERR_FILENO);

        execl("zonk/zonk", "zonk/zonk", NULL);

        return 0;
    }
    //close unused pipe ends
    close(outpipefd[0]);
    close(inpipefd[1]);

    init_game(to_game, from_game, max_score, players, p_num);  

    // chat and play game
    int game_fd;
    chat_and_play_game(
        fds, 
        to_game, from_game,
        &current_player, 
        players, p_num,
        max_score
    );

    // close server
    kill(pid, SIGINT);

    // free tokens and players array
    free_tokens(tokens, p_num);
    free_players(players, PLAYERS_NUM);

    return 0;
}
        
