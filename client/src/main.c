#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <signal.h>

#include <sys/socket.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "tools.h"


int sock_fd;

void
handle_shutdown(int signal)
{
     if (sock_fd)
         close(sock_fd);
  
     perror(" >> Interrupted >> Socket closed ");
     exit(0);
}


int
main(void)
{
    // handle CTRL+C
    if (catch_signal(SIGINT, handle_shutdown) == -1)
    {
        perror("catch_signal SIGINT");
        exit(1);
    }

    // scan token
    char input[80];

    int num;
    char* port;
    char* key;
    char* ip;

    printf("token from host: ");
    if (scanf("%s", &input) == 0)
        error("Wrong input: token");
    
    sscanf(input, "%d#%m[^#]#%m[^#]#%m[^$]$", &num, &key, &ip, &port);
    
    
    // connect to host 
    int status;
    struct addrinfo hints;
    struct addrinfo *res;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    status = getaddrinfo(ip, port, &hints, &res);
    if (status == -1)
    {
        printf("getaddrinfo: %s\n", strerror(errno));
        error("getaddrinfo");
    }

        //  socket / connect
    struct addrinfo* p;
    for (p = res; p != NULL; p = p->ai_next)
    {
        sock_fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if (sock_fd == -1)
        {
            perror("socket");
            continue;
        }

        int c = connect(sock_fd, res->ai_addr, res->ai_addrlen);
    }


    // scan name
    printf("Enter your name: ");
    if (scanf("%s", &input) == 0)
        error("Wrong input: name");
    printf("\n");
    printf("Name: %s\n", input);

    // init dialogue
    char connect_msg[4095];
    memset(connect_msg, 0, sizeof(connect_msg));
    snprintf(connect_msg, sizeof(connect_msg), "%s:%s \n", key, input);
    say(sock_fd, connect_msg); 

    printf("Print 'exit' to exit.\n");

    pid_t parent_pid = getpid();
    pid_t pid = fork();
    
    if (!pid) 
    { // child reads from server

        int stop_counter = 0;
        while (true)
        {
                stop_counter++;

                memset(connect_msg, 0, sizeof(connect_msg));
                int rc = read_in(sock_fd, connect_msg, sizeof(connect_msg));
                if (rc > 0)
                    stop_counter = 0;
                else if (stop_counter > 30)
                {
                    printf("HOST DISCONNECTED! FINISHING PROCESS...\n");
                    kill(parent_pid, SIGINT);
                    break;
                }

                printf(">> FROM GAME:\n%s\n", connect_msg);

                if (strstr(connect_msg, "WINS"))
                {
                    kill(parent_pid, SIGINT);
                    break;
                }
                    
        }

        // free
        freeaddrinfo(res);
        free(key);
        free(ip);
        free(port);
        return 0;
    }

    // parent writes to server
    while (true)
    {    
        memset(input, 0, sizeof(input));

        sleep(1);
        printf("> ");
        if (scanf("%s", &input) == 0)
            continue;

        if (!strcmp(input, "exit"))
        {
            kill(pid, SIGINT);
            break;
        }

        memset(connect_msg, 0, sizeof(connect_msg));
        snprintf(connect_msg, sizeof(connect_msg), "%s\n\n", input);
        say(sock_fd, connect_msg); 
    }
        
    // free
    freeaddrinfo(res);
    free(key);
    free(ip);
    free(port);
    
    // free sockets
    close(sock_fd);

    return 0;
}
