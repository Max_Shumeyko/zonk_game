NAME = Zonk_game
CC = gcc
CFLAGS?=-Wall -Wextra -Wpedantic -std=gnu11

all: 
	make -C zonk
	make -C host
	make -C client
	$(CC) main.c $(CFLAGS) -o $(NAME)

clean:
		rm $(NAME)
		make clean -C zonk
		make clean -C host
		make clean -C client
