## Description

Here is the zonk game - a popular student cubes game. 
Tested on Linux only!

Rules: https://en.wikipedia.org/wiki/Farkle (or google 'Зонк')

Here you can:
1) Play single or hot-seat. Just run the original zonk/zonk.
2) Play online. Run host, input game conditions and get tokens. 
Then send tokens to friends so they can connect using client app or client app. 

Hint: use telnet like:

token structure is 'NUM#KEY#IP_ADDR#PORT$
* .> telnet IP_ADDR PORT
* .> KEY:YOUR_NAME
(for now telnet client seems more stable)

## Installation
.> make



