#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <errno.h>


#define FLUSH_PRINT(...) ({\
    printf(__VA_ARGS__);\
    fflush(stdout);\
})


int*
throw_cubes(int n)
{
    static int cubes[6];
    for (int i = 0; i < 6; i++)
    {
        cubes[i] = 0;
        if (i < n)
            cubes[i] = rand() % 6 + 1;
    }

    return cubes;
}


void
show_cubes(int* cubes)
{
    for (int i = 0; i < 6; i++)
        if(cubes[i]) 
            FLUSH_PRINT("CUBE: %d\n", cubes[i]);
}


bool
all(int* array, int size)
{
    for (int i = 0; i < size; i++)
        if (!array[i])
            return false;

    return true;
}


int*
count_distr(int* cubes)
{
    static int score_distribution[6];
    for (int i = 0; i < 6; i++)
        score_distribution[i] = 0;
    for (int i = 0; i < 6; i++)
        score_distribution[cubes[i] - 1]++;

    return score_distribution;
}


typedef struct {
    int scores;
    bool zonk;
    bool addition_turn;
    int cubes_played;
} turn_resul_t;
    
turn_resul_t
count_scores(int* cubes, int size)
{
    int scores = 0;
    int* cubes_distr = count_distr(cubes);

    // check all different
    if (all(cubes_distr, 6))
        return (turn_resul_t){
            .scores=1500,
            .addition_turn=true,
        };

    // check if 3 pairs
    int pairs = 0;
    for (int i = 0; i < 6; i++)
        if (cubes_distr[i] == 2)
            pairs += 1;
    if (pairs == 3)
        return (turn_resul_t){
            .scores=750,
            .addition_turn=true
        };

    int no_scores_cubes = 0;
    bool addition_turn = false;
    for (int i = 0; i < 6; i++)
        // check for 3 cubes
        if (cubes_distr[i] >= 3) {
            int coef = i > 0 ? 100 : 1000;
            scores += (i + 1) * coef * (cubes_distr[i] - 2);
        }
        // check for 2- and 5- scores cubes
        else if (i + 1 == 2)
            scores += 50 * cubes_distr[i];
        else if (i + 1 == 5)
            scores += 100 * cubes_distr[i];
        else 
            no_scores_cubes += cubes_distr[i];

        if (no_scores_cubes == size)
            return (turn_resul_t){.zonk=true};
        else if (no_scores_cubes == 0)
            addition_turn = true;

    return (turn_resul_t){
        .scores=scores,
        .cubes_played=size - no_scores_cubes,
        .addition_turn=addition_turn,
    };
}
        

void 
praise_the_winner(char* player_name, int max_score) 
{ 
    for (int i = 0; i < 10; i++)
        FLUSH_PRINT("$");
    FLUSH_PRINT(
         "  PLAYER %s WINS WITH %d SCORES!!! ",
         player_name,
         max_score
    );
    for (int i = 0; i < 10; i++)
        FLUSH_PRINT("$");
    FLUSH_PRINT("\n\n");
}


typedef struct {
    int players_num;    
    int max_score;
    char** names;
} game_condition_t;

game_condition_t*
init_game(game_condition_t* g_c)
{
    FLUSH_PRINT("Set the number of players [1-10]: ");
    char buf[4];
    scanf("%s", &buf);
    int val = atoi(buf);
    if (val == 0) {
        FLUSH_PRINT("Wrong input!\n");
        return NULL;
    }
    else 
        g_c->players_num = val;

    int i = g_c->players_num;
    if ( (i < 1) || (i > 10) ) {
        FLUSH_PRINT("Wrong number of players! Try again!\n");
        return NULL;
    }

    g_c->names = malloc(g_c->players_num * sizeof(char*));
    FLUSH_PRINT("PLAYERS NUM: %d\n", g_c->players_num);
    for (i = 0; i < g_c->players_num; i++)
    {
        FLUSH_PRINT("player's %d name: ", i);
        scanf("%ms", &g_c->names[i]);
    }
    
    FLUSH_PRINT("Set scores limit for the game [1000-30000]: ");
    scanf("%s", &buf);
    val = atoi(buf);
    if (val == 0) {
        FLUSH_PRINT("Wrong input!\n");
        return NULL;
    }
    else 
    {
        if ( (val < 1000) || (val > 30000) ) {
            FLUSH_PRINT("Wrong score limit! Try again!\n");
            return NULL;
        }
        g_c->max_score = val;

    }

    return g_c;
}


void
show_score_table(int* scores, game_condition_t cond)
{
    int i;
    for (i = 0; i < 10; i++)
        FLUSH_PRINT("#");
    FLUSH_PRINT("\n");
    FLUSH_PRINT("\nSCORE TABLE:\n");
//    for (i = 0; i < 30; i++)
//        FLUSH_PRINT("#");
    for (int i = 0; i < cond.players_num; i++)
        FLUSH_PRINT("PLAYER %s: %d\n", cond.names[i], scores[i]);
    for (i = 0; i < 10; i++)
        FLUSH_PRINT("#");
    FLUSH_PRINT("\n");
    FLUSH_PRINT("\n");
}


void 
release_game_condition(game_condition_t* g_c)
{
    for (int i = 0; i < g_c->players_num; i++)
        free(g_c->names[i]);
    free(g_c->names);
}

void
game_circle(game_condition_t* game_cond)
{
    // PLAYERS TURNS, game circle
    char command[80];
    int max_score = 0;
    int nsize = game_cond->players_num * sizeof(int);
    int* players_scores = malloc(nsize);
    memset(players_scores, 0, nsize); 
    int player_id = -1;

    while (max_score < game_cond->max_score)
    {
        int cubes_num = 6;

        // pass the turn to next player
        player_id++;
        if (player_id > game_cond->players_num - 1)
            player_id = 0;
        FLUSH_PRINT("NEXT TURN: '%s' \n", game_cond->names[player_id]);

        // play 
        int round_scores = 0;
        while (cubes_num > 0)
        {
            FLUSH_PRINT("Do you want to throw cubes? [y/n]\n"); 
            scanf("%s", command);
            
            if (!strcmp(command, "y"))
            {
                int* cubes = throw_cubes(cubes_num);
                show_cubes(cubes);
                turn_resul_t tres = count_scores(cubes, cubes_num);
                FLUSH_PRINT(
                    "SCORES: %d; ZONK: %s; ADDITIONAL TURN: %s"
                    ", CUBES PLAYED: %d\n", 
                    tres.scores,
                    tres.zonk ? "true" : "false",
                    tres.addition_turn ? "true" : "false",
                    tres.cubes_played
                );
                    
                // ZONK elliminates all round's scores
                if (tres.zonk) {
                    round_scores = 0; 
                    break;
                }

                // add scores to the player                    
                round_scores += tres.scores;

                if (tres.addition_turn)
                    cubes_num = 6;
                else
                    cubes_num -= tres.cubes_played;
            }
            else if (!strcmp(command, "n"))
                break;

        } // while cubes_num > 0
        players_scores[player_id] += round_scores;
        if(players_scores[player_id] > max_score)
            max_score = players_scores[player_id]; 
            
        show_score_table(players_scores, *game_cond);
   } // while max_score
        
   praise_the_winner(game_cond->names[player_id], max_score);

   free(players_scores);
}


int
main(int argc, char* argv[])
{
    srand(time(NULL));

    char command[80];
    int i;
    int* cubes;
    game_condition_t game_cond;

    while (true)
    {
        FLUSH_PRINT("Print 'exit' to exit. 'y' to continue.\n");
        fflush(stdout);
        scanf("%s", command);
        if (!strcmp(command, "exit"))
            exit(0);

        // init game conditions: players number and max score
        i = 0;
        while (true)
            if(init_game(&game_cond) != NULL) 
                break;

        FLUSH_PRINT(
            "GAME CONDITIONS: NUM PLAYERS = %d; SCORE LIMIT = %d\n",
            game_cond.players_num,
            game_cond.max_score
        );

        game_circle(&game_cond);
        release_game_condition(&game_cond);
    } // true

    return 0;
}
        
