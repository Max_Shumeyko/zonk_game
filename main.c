#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>


const char rules[] = "Welcome to Zonk game!\n"
    "Rules: https://en.wikipedia.org/wiki/Farkle (or google 'Зонк')\n\n"
    "Here you can: \n"
    "   1) Play single or hot-seat. Just run the original zonk/zonk.\n"
    "   2) Play online. Run host, input game conditions and get tokens. \n"
    "\tThen send tokens to friends so they can connect using client app or telnet.\n\n"
    "Hint: use telnet like:\n"
    "\ttoken structure is 'NUM#KEY#IP_ADDR#PORT$'\n"
    "\t> telnet IP_ADDR PORT\n\t> KEY:YOUR_NAME\n"
    "\t for now telnet client seems more stable.\n";


int
main(void)
{
    printf("%s\n", rules);

    while (true)
    {
        printf(
            "1) Play hot-seat or single.\n"
            "2) Run game online. Here you get init game and get tokens.\n"
            "\tThen start in new window and 'Join'.\n"
            "3) Join. Input the token and play ^__^.\n"
        );

        
        char buf[80];
        printf("\nCHOOSE 1/2/3/help/exit: ");
        scanf("%s", buf);
        
        if (!strcmp(buf, "exit"))
            return 0;
        else if (!strcmp(buf, "1"))
            system("zonk/zonk");
        else if (!strcmp(buf, "2"))
            system("host/host_app");
        else if (!strcmp(buf, "3"))
            system("client/client_app");
        else if (!strcmp(buf, "help"))
            printf("%s\n", rules);

    }
    
    
    return 0;
}
